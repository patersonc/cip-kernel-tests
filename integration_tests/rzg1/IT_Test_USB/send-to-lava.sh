##!/bin/sh
echo "running send-to-lava.sh"
RESULT_FILE="$1"

#cat $RESULT_FILE
if [ -f "${RESULT_FILE}" ]; then
	while read -r line; do
		test=`echo "${line}" | awk -F+ '{print $1}'`
		result=`echo "${line}" | awk -F+ '{print $2}'`
		lava-test-case "${test}" --result "${result}"
		if [ $result = "fail" ]; then
			echo "${line}" | awk -F+ '{print $3}'
		fi
	done < "${RESULT_FILE}"

	echo "sent result from txt file"
	echo "print log_file.txt for debug purpose"
	cat $RESULT_FILE

else
	echo "WARNING: result file is missing!"
fi
