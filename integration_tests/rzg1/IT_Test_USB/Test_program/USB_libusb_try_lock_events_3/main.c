/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking libusb_try_lock_events system call
 * Sequence: libusb_init();libusb_try_lock_events();libusb_exit();
 * Testing level: system call
 * Test-case type: Abnormal
 * Expected result: NG
 * Name: main.c
 * Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: External device: USB Storage. Condition: Call API libusb_try_lock_events of usblib library after libusb_exit(). Expected result = NG
 */
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	int result;
	struct libusb_context *ctx;

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Main checking item
	libusb_init(&ctx);
	if (result < 0) {
		printf("Error: libusb_init");
		return 0;
	}

	libusb_exit(ctx);

	result = libusb_try_lock_events(ctx); 	// Expected_value 1, the lock was not obtained

	if(result == 0)
		printf ("OK\n");
	else if (result == 1)
		printf ("NG\n");

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


